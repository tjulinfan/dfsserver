namespace java com.flyflyaway.protocol.client2namenode

struct DataNodeDescriptor {
    1: string ipAddress,
    2: i32 port
}

struct Block {
    1: string id,
    2: i64 size,
    3: list<DataNodeDescriptor> dataNodeList
}

service Client2NameNodeProtocol {

    list<Block> createFile(1:string path, 2:i64 size, 3:i64 blockSize),

    list<Block> getFile(1:string path),

    void completeBlockTransmission(1:string blockId)
}