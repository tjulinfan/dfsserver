namespace java com.flyflyaway.protocol.datanode2namenode

/**
 * Information about the datanode
 *
 * ipAddress: IP address of the datanode
 * capacity:  byte unit
 * transPort: port which be used to transformission file data
 */
struct DataNodeInfo {
    1: string ipAddress,
    2: i32 transPort,
    3: i64 capacity
}

struct DataNodeRegistration {
    1: string ipAddress,
    2: i32 transPort,
    3: i64 capacity
}

struct BlockOperation {
    1: i16 op,
    2: string blockId,
    3: string targetHost,
    4: i32 targetPort
}

service DataNode2NameNodeProtocol {

    /**
     * Register to NameNode
     *
     * dataNodeRegistration: information about that datanode
     */
    void registerDataNode(1:DataNodeRegistration dataNodeRegistration),

    /**
     * Send heart-beat message to NameNode, and then receive some operations
     *
     * dataNodeInfo:
     */
    list<BlockOperation> ping(1:DataNodeInfo dataNodeInfo),

    /**
     * When datanode finishes the block transmission, it will invoke this method
     *
     * blockId:
     */
    void completeBlockTransmission(1:string blockId)

}