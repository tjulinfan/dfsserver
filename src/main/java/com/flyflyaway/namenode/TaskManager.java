package com.flyflyaway.namenode;

import com.flyflyaway.protocol.datanode2namenode.Operation;

import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by LinFan on 16/1/13.
 */
public class TaskManager {
    private static TaskManager instance;

    public static TaskManager getInstance() {
        if (instance == null) {
            instance = new TaskManager();
        }
        return instance;
    }

    private ConcurrentHashMap<String, Vector<Operation>> tasksOfDataNodes;

    private TaskManager() {
        tasksOfDataNodes = new ConcurrentHashMap<String, Vector<Operation>>();
    }

    public void addTask(String dataNodeIp, Operation operation) {
        if (!tasksOfDataNodes.containsKey(dataNodeIp)) {
            tasksOfDataNodes.put(dataNodeIp, new Vector<Operation>());
        }
        getTasks(dataNodeIp).add(operation);
    }

    public Vector<Operation> getTasks(String dataNodeIp) {
        return tasksOfDataNodes.get(dataNodeIp);
    }
}
