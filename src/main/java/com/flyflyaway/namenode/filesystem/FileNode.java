package com.flyflyaway.namenode.filesystem;

/**
 * Created by LinFan on 16/1/17.
 */
public abstract class FileNode {

    protected String name;

    protected FileNode parent;
    protected boolean isDirectory;

    protected FileNode(String name, FileNode parent, boolean isDirectory) {
        this.name = name;
        this.parent = parent;
        this.isDirectory = isDirectory;
    }

    protected FileNode(String name, FileNode parent) {
        this(name, parent, false);
    }

    public FileNode getParent() {
        return parent;
    }

    public void setParent(FileNode parent) {
        this.parent = parent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDirectory() {
        return isDirectory;
    }

    public String getPath() {
        return parent == null ? name : parent.getPath() + "/" + name;
    }
}
