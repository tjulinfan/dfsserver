package com.flyflyaway.namenode.filesystem;

import java.util.List;

/**
 * Created by LinFan on 16/1/17.
 */
public class File extends FileNode {

    private List<Block> blocks;
    private long size;
    private long blockSize;

    protected File(String name, long size, FileNode parent, long blockSize) {
        super(name, parent);

        this.size = size;
        this.blockSize = blockSize;
        genBlocks();
    }

    protected File(String name, long size, FileNode parent) {
        this(name, size, parent, FileSystem.DEFAULT_BLOCK_SIZE);
    }

    protected File(String name, long size, long blockSize) {
        this(name, size, null, blockSize);
    }

    protected File(String name, long size) {
        this(name, size, null);
    }

    private void genBlocks() {
        this.blocks = BlockManager.getInstance().createBlocksForFile(this);
    }

    public long getSize() {
        return size;
    }

    public long getBlockSize() {
        return blockSize;
    }

    public List<Block> getBlocks() {
        return blocks;
    }
}
