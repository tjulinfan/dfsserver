package com.flyflyaway.namenode.filesystem;

import com.flyflyaway.namenode.DataNodeDescriptor;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.List;
import java.util.Vector;

/**
 * Created by LinFan on 16/1/17.
 */
public class Block {

    public static final int BLOCK_ID_LEN = 32;

    public enum State {
        INITIALIZED(1),
        ASSIGNED(2),
        BACKEDUP(3),
        STABLE(4);

        private final int value;
        State(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    private String id;
    private State state;
    private long size;
    private File file;
    private List<DataNodeDescriptor> dataNodeList;

    public Block(long size) {
        this.size = size;
        this.id = RandomStringUtils.random(BLOCK_ID_LEN, true, true);
        this.state = State.INITIALIZED;

        this.dataNodeList = new Vector<>();
    }

    public String getId() {
        return id;
    }

    public State getState() {
        return state;
    }

    public long getSize() {
        return size;
    }

    public List<DataNodeDescriptor> getDataNodeList() {
        return dataNodeList;
    }

    public void assignToFile(File file) {
        this.file = file;
    }

    public void addDataNodes(List<DataNodeDescriptor> dataNodes) {
        dataNodeList.addAll(dataNodes);
    }
}
