package com.flyflyaway.namenode.filesystem;

import com.flyflyaway.namenode.DataNodeManager;

import java.util.List;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by LinFan on 16/1/16.
 */
public class BlockManager {

    public static final int BACK_UP_FACTORY = 3;

    private static final BlockManager instance = new BlockManager();

    private ConcurrentHashMap<String, Block> blockMap;

    private BlockManager() {
        blockMap = new ConcurrentHashMap<>();
    }

    public static final BlockManager getInstance() {
        return instance;
    }

    public Block createBlock(long size) {
        Block newBlock = new Block(size);
        chooseDataNodesForBlock(newBlock);
        blockMap.put(newBlock.getId(), newBlock);
        return newBlock;
    }

    private void chooseDataNodesForBlock(Block block) {
        block.addDataNodes(DataNodeManager.getInstance().pickDataNodesForBlock(block, BACK_UP_FACTORY));
    }

    public List<Block> createBlocksForFile(File file) {
        List<Block> blocks = new Vector<>();
        long fileSize = file.getSize(), blockSize = file.getBlockSize();
        for (int i = 0; i < fileSize / blockSize; ++i) {
            Block newBlock = createBlock(blockSize);
            newBlock.assignToFile(file);
            blocks.add(newBlock);
        }
        if (fileSize % blockSize != 0) {
            Block lastBlock = createBlock(fileSize % blockSize);
            lastBlock.assignToFile(file);
            blocks.add(lastBlock);
        }
        return blocks;
    }
}
