package com.flyflyaway.namenode.filesystem;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by LinFan on 16/1/17.
 */
public class Directory extends FileNode {

    private ConcurrentHashMap<String, FileNode> files;

    protected Directory(String name, FileNode parent) {
        super(name, parent, true);

        files = new ConcurrentHashMap<>();
    }

    protected Directory(String name) {
        this(name, null);
    }

    public void addFile(FileNode file) {
        files.put(file.getName(), file);
        file.setParent(this);
    }

    public FileNode getFile(String name) {
        return files.get(name);
    }
}
