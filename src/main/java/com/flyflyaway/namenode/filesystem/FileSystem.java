package com.flyflyaway.namenode.filesystem;

/**
 * Created by LinFan on 16/1/16.
 */
public class FileSystem {

    public static final long DEFAULT_BLOCK_SIZE = 1024 * 1024 * 64;

    private static final FileSystem instance = new FileSystem();

    private Directory root;

    private FileSystem() {
        root = new Directory("");
    }

    public static FileSystem getInstance() {
        return instance;
    }

    public File getFile(String filePath) {
        // TODO: handle invalid filePath
        String[] paths = filePath.split("/");
        if (paths.length == 0) {
            return null;
        }

        Directory currentDir = root;
        for (int i = 1; i < paths.length; ++i) {
            FileNode nextDir = currentDir.getFile(paths[i]);
            if (nextDir == null) {
                return null;
            }
            if (nextDir.isDirectory()) {
                currentDir = (Directory)nextDir;
            } else {
                return (File)(i == paths.length - 1 ? nextDir : null);
            }
        }
        return null;
    }

    public File createFile(String filePath, long fileSize, long blockSize) {
        // TODO: handle invalid filePath
        String[] paths = filePath.split("/");
        if (paths.length == 0) {
            return null;
        }

        Directory currentDir = root;
        for (int i = 1; i < paths.length - 1; ++i) {
            FileNode nextDir = currentDir.getFile(paths[i]);
            if (nextDir == null) {
                currentDir.addFile(nextDir = new Directory(paths[i], currentDir));
            }
            if (nextDir.isDirectory()) {
                currentDir = (Directory)nextDir;
            } else {
                return null;
            }
        }
        File newFile = new File(paths[paths.length - 1], fileSize, blockSize);
        currentDir.addFile(newFile);
        return newFile;
    }

    public File createFile(String filePath, long fileSize) {
        return createFile(filePath, fileSize, DEFAULT_BLOCK_SIZE);
    }
}
