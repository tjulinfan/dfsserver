package com.flyflyaway.namenode.client2namenode;

import com.flyflyaway.namenode.filesystem.File;
import com.flyflyaway.namenode.filesystem.FileSystem;
import com.flyflyaway.protocol.client2namenode.Block;
import com.flyflyaway.protocol.client2namenode.Client2NameNodeProtocol;
import org.apache.thrift.TException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LinFan on 16/1/15.
 */
public class ProtocolHandler implements Client2NameNodeProtocol.Iface {

    private static List<Block> convertDFSBlocksToBlocks(List<com.flyflyaway.namenode.filesystem.Block> dfsBlocks) {
        List<Block> blocks = new ArrayList<>();
        for (com.flyflyaway.namenode.filesystem.Block dfsBlock : dfsBlocks) {
            blocks.add(new DFSBlockWrapper(dfsBlock));
        }
        return blocks;
    }

    @Override
    public List<Block> createFile(String path, long size, long blockSize) throws TException {
        System.out.println("===============================");
        System.out.println("CreateFile");
        System.out.println(path + " " + size + " " + blockSize);
        FileSystem fileSystem = FileSystem.getInstance();
        File newFile = fileSystem.createFile(path, size, blockSize);
        return convertDFSBlocksToBlocks(newFile.getBlocks());
    }

    @Override
    public List<Block> getFile(String path) throws TException {
        FileSystem fileSystem = FileSystem.getInstance();
        return convertDFSBlocksToBlocks(fileSystem.getFile(path).getBlocks());
    }

    @Override
    public void completeBlockTransmission(String blockId) throws TException {

    }
}
