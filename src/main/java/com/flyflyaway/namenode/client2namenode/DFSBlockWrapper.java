package com.flyflyaway.namenode.client2namenode;

import com.flyflyaway.namenode.DataNodeDescriptor;
import com.flyflyaway.namenode.filesystem.Block;

import java.util.ArrayList;

public class DFSBlockWrapper extends com.flyflyaway.protocol.client2namenode.Block {
    public DFSBlockWrapper(Block dfsBlock) {
        this.id = dfsBlock.getId();
        this.size = dfsBlock.getSize();
        this.dataNodeList = new ArrayList<>();
        for (DataNodeDescriptor dataNodeDescriptor : dfsBlock.getDataNodeList()) {
            if (dataNodeDescriptor == null) {
                System.out.println("HFHHFHHFHFHFFH");
                this.dataNodeList.add(null);
            } else {
                this.dataNodeList.add(new DataNodeDescriptorWrapper(dataNodeDescriptor));
            }
        }
    }
}
