package com.flyflyaway.namenode.client2namenode;

import com.flyflyaway.namenode.DataNodeDescriptor;

/**
 * Created by LinFan on 16/1/18.
 */
public class DataNodeDescriptorWrapper extends com.flyflyaway.protocol.client2namenode.DataNodeDescriptor {

    public DataNodeDescriptorWrapper(DataNodeDescriptor dataNodeDescriptor) {
        super(dataNodeDescriptor.getIpAddress(),
                dataNodeDescriptor.getTransPort());
    }

}
