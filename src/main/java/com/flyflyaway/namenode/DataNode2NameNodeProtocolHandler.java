package com.flyflyaway.namenode;

import com.flyflyaway.protocol.datanode2namenode.*;
import org.apache.thrift.TException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LinFan on 16/1/12.
 */
public class DataNode2NameNodeProtocolHandler implements DataNode2NameNodeProtocol.Iface {

    private DataNodeManager dataNodeManager;

    public DataNode2NameNodeProtocolHandler() {
        dataNodeManager = DataNodeManager.getInstance();
    }

    @Override
    public void registerDataNode(DataNodeRegistration dng) throws TException {
        // TODO: implementation
        System.out.println("===========================");
        System.out.println("Register by " + dng.getIpAddress());
        System.out.println("PORT: " + dng.getTransPort());
        System.out.println("CAP: " + dng.getCapacity());

        DataNodeDescriptor newNode = new DataNodeDescriptor(dng.getIpAddress(),
                dng.getTransPort(),
                dng.getCapacity());
        dataNodeManager.addDataNode(newNode);

        System.out.println(dataNodeManager.getDataNodeCount());
    }

    @Override
    public List<BlockOperation> ping(DataNodeInfo dataNodeInfo) throws TException {
        // TODO: implementation
        dataNodeManager.updateDataNode(dataNodeInfo.getIpAddress() + ":" + dataNodeInfo.getTransPort());
        return new ArrayList();
    }

    @Override
    public void completeBlockTransmission(String blockId) throws TException {
        // TODO: implementation
        System.out.println("===========================");
        System.out.println("Complete block transmission: " + blockId);
    }
}
