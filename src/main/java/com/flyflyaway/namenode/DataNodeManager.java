package com.flyflyaway.namenode;

import com.flyflyaway.namenode.filesystem.Block;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by LinFan on 16/1/12.
 */
public class DataNodeManager {

    public static long DATANODE_TIMEOUT = 10 * 1000;
    private static final DataNodeManager instance = new DataNodeManager();

    public static DataNodeManager getInstance() {
        return instance;
    }

    private ConcurrentHashMap<String, DataNodeDescriptor> dataNodes;

    private DataNodeManager() {
        dataNodes = new ConcurrentHashMap<>();
    }

    public int getDataNodeCount() {
        return dataNodes.size();
    }

    public synchronized void addDataNode(DataNodeDescriptor dataNode) {
        dataNodes.put(dataNode.getName(), dataNode);
    }

    public void removeDataNode(String dataNodeName) {
        dataNodes.remove(dataNodeName);
    }

    public boolean isNodeExisted(String dataNodeName) {
        return dataNodes.containsKey(dataNodeName);
    }

    public DataNodeDescriptor getDataNode(String dataNodeName) {
        return dataNodes.get(dataNodeName);
    }

    // TODO: datanode not exsited
    public void updateDataNode(String dataNodeName) {
        if (isNodeExisted(dataNodeName)) {
            getDataNode(dataNodeName).update();
        }
    }

    public List<DataNodeDescriptor> pickDataNodesForBlock(Block block, int count, List<DataNodeDescriptor> excluded) {
        List<DataNodeDescriptor> pickedDataNodes = new ArrayList<>();

        // Find all available datanodes
        List<DataNodeDescriptor> allNodes = new ArrayList<>();
        checkFailedNodes();
        for (Map.Entry<String, DataNodeDescriptor> entry : dataNodes.entrySet()) {
            DataNodeDescriptor dataNode = entry.getValue();
            if (dataNode.getCapacity() > block.getSize() &&
                    !excluded.contains(dataNode)) {
                allNodes.add(dataNode);
            }
        }

        // pick datanode randomly
        Random random = new Random();
        for (int i = 0; i < count; ++i) {
            DataNodeDescriptor pickedNode = null;
            if (!allNodes.isEmpty()) {
                pickedNode = allNodes.get(random.nextInt(allNodes.size()));
                allNodes.remove(pickedNode);
            }
            pickedDataNodes.add(pickedNode);
        }

        return pickedDataNodes;
    }

    public List<DataNodeDescriptor> pickDataNodesForBlock(Block block, int count) {
        return pickDataNodesForBlock(block, count, new ArrayList<DataNodeDescriptor>());
    }

    public List<DataNodeDescriptor> checkFailedNodes() {
        ArrayList<DataNodeDescriptor> failedNodes = new ArrayList<>();
        long currentTimestamp = System.currentTimeMillis();
        for (Map.Entry<String, DataNodeDescriptor> entry : dataNodes.entrySet()) {
            if (currentTimestamp - entry.getValue().getLastUpdateTimestamp() > DATANODE_TIMEOUT) {
                failedNodes.add(entry.getValue());
                removeDataNode(entry.getKey());
            }
        }
        return failedNodes;
    }
}
