package com.flyflyaway.namenode;

import com.flyflyaway.namenode.client2namenode.ProtocolHandler;
import com.flyflyaway.protocol.client2namenode.Client2NameNodeProtocol;
import com.flyflyaway.protocol.datanode2namenode.DataNode2NameNodeProtocol;
import com.flyflyaway.util.Config;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TServerTransport;
import org.apache.thrift.transport.TTransportException;

public class NameNode {

    public static final int D2NP_PORT = Config.getInt("namenode_d2np_port");
    public static final int C2NP_PORT = Config.getInt("namenode_c2np_port");

    private DataNode2NameNodeProtocolHandler d2npHandler;
    private DataNode2NameNodeProtocol.Processor d2npProcessor;

    private ProtocolHandler c2npHandler;
    private Client2NameNodeProtocol.Processor c2npProcessor;

    private void startDataNode2NameNodeProtocol() {
        d2npHandler = new DataNode2NameNodeProtocolHandler();
        d2npProcessor = new DataNode2NameNodeProtocol.Processor(d2npHandler);
        final Runnable serverRunnable = new Runnable() {
            public void run() {
                try {
                    TServerTransport serverTransport = new TServerSocket(D2NP_PORT);
                    TThreadPoolServer.Args args = new TThreadPoolServer.Args(serverTransport).processor(d2npProcessor);
                    TServer server = new TThreadPoolServer(args);
                    System.out.println("Start the d2np server...");
                    server.serve();
                } catch (TTransportException e) {
                    // TODO: handle the exception
                    e.printStackTrace();
                }
            }
        };

        new Thread(serverRunnable).start();
    }

    private void startClient2NameNodeProtocol() {
        c2npProcessor = new Client2NameNodeProtocol.Processor(new ProtocolHandler());
        final Runnable serverRunnable = new Runnable() {
            @Override
            public void run() {
                try {
                    TServerTransport serverTransport = new TServerSocket(C2NP_PORT);
                    TThreadPoolServer.Args args = new TThreadPoolServer.Args(serverTransport)
                            .processor(c2npProcessor);
                    TServer server = new TThreadPoolServer(args);
                    System.out.println("Start the c2np server...");
                    server.serve();
                } catch (TTransportException e) {
                    // TODO: handler the exception
                    e.printStackTrace();
                }
            }
        };

        new Thread(serverRunnable).start();
    }

    public void start() {
        startDataNode2NameNodeProtocol();
        startClient2NameNodeProtocol();
    }
}
