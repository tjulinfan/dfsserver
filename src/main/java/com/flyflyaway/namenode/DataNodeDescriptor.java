package com.flyflyaway.namenode;

/**
 * Created by LinFan on 16/1/12.
 */
public class DataNodeDescriptor {


    private String ipAddress;
    private long capacity;
    private int transPort;
    private long lastUpdateTS;

    public DataNodeDescriptor(String ipAddress, int transPort, long capacity) {
        this.ipAddress = ipAddress;
        this.capacity = capacity;
        this.transPort = transPort;
        this.lastUpdateTS = System.currentTimeMillis();
    }

    public String getName() {
        return ipAddress + ":" + transPort;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public long getCapacity() {
        return capacity;
    }

    public void setCapacity(long capacity) {
        this.capacity = capacity;
    }

    public int getTransPort() {
        return transPort;
    }

    public void update() {
        this.update(System.currentTimeMillis());
    }

    public void update(long timestamp) {
        this.lastUpdateTS = timestamp;
    }

    public long getLastUpdateTimestamp() {
        return lastUpdateTS;
    }
}
