package com.flyflyaway.namenode;

import com.flyflyaway.namenode.filesystem.Block;
import com.flyflyaway.namenode.filesystem.File;
import com.flyflyaway.namenode.filesystem.FileSystem;

/**
 * Created by LinFan on 16/1/16.
 */
public class testDFSFile {

    public static void main(String[] args) {
        test();
    }

    public static void test() {
        FileSystem fs = FileSystem.getInstance();

        File newFile = fs.createFile("/test1/test2/test3", 100);
        System.out.println(newFile.getPath());
        System.out.println(newFile.getBlocks().size());

        for (Block block : newFile.getBlocks()) {
            for (DataNodeDescriptor dataNode : block.getDataNodeList()) {
                System.out.println(block.getId() + "    " + dataNode);
            }
        }

        File newFile2 = fs.createFile("/test1/test2/test4", 100000);
        System.out.println(newFile2.getPath());
        System.out.println(newFile2.getBlocks().size());

        for (Block block : newFile2.getBlocks()) {
            for (DataNodeDescriptor dataNode : block.getDataNodeList()) {
                System.out.println(block.getId() + "    " + dataNode);
            }
        }

        System.out.println(newFile.getParent() == newFile2.getParent());
    }
}
