package com.flyflyaway.exception;

import com.flyflyaway.exception.DFSException;

/**
 * Created by LinFan on 16/1/15.
 */
public class FileNotFoundException extends DFSException {
    public FileNotFoundException(String fileName, String parentPath) {
        super(fileName + " not found in " + parentPath);
    }
}
