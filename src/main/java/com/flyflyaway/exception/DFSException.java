package com.flyflyaway.exception;

/**
 * Created by LinFan on 16/1/15.
 */
public class DFSException extends Exception {
    public DFSException() {
        super();
    }

    public DFSException(String message) {
        super(message);
    }

    public DFSException(Throwable cause) {
        super(cause);
    }

    public DFSException(String message, Throwable cause) {
        super(message, cause);
    }

    public DFSException(Exception exception) {
        super(exception);
    }
}
