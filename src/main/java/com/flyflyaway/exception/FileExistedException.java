package com.flyflyaway.exception;

import com.flyflyaway.exception.DFSException;

/**
 * Created by LinFan on 16/1/15.
 */
public class FileExistedException extends DFSException {
    public FileExistedException(String fileName, String parentPath) {
        super(fileName + " has existed in " + parentPath);
    }
}
