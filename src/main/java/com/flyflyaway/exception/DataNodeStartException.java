package com.flyflyaway.exception;

/**
 * Created by LinFan on 16/1/17.
 */
public class DataNodeStartException extends DFSException {
    public DataNodeStartException(Exception exception) {
        super(exception);
    }
}
