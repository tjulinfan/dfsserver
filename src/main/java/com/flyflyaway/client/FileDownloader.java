package com.flyflyaway.client;

import com.flyflyaway.protocol.client2namenode.Block;
import com.flyflyaway.protocol.client2namenode.Client2NameNodeProtocol;
import com.flyflyaway.protocol.client2namenode.DataNodeDescriptor;
import com.flyflyaway.util.Config;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class FileDownloader {

    private static final String NAMENODE_HOST = Config.getString("namenode_host");
    private static final int NAMENODE_PORT = Config.getInt("namenode_c2np_port");

    private TTransport transport;
    private Client2NameNodeProtocol.Client client2NameNodeProtocol;
    private String remoteFilePath, localFilePath;

    public FileDownloader(String remoteFilePath, String localFilePath) {
        this.remoteFilePath = remoteFilePath;
        this.localFilePath = localFilePath;
    }

    private void connectToNameNode() throws TTransportException {
        transport = new TSocket(NAMENODE_HOST, NAMENODE_PORT);
        transport.open();
        client2NameNodeProtocol = new Client2NameNodeProtocol.Client(new TBinaryProtocol(transport));
    }

    private void closeConnectionWithNameNode() {
        transport.close();
    }

    public void start() throws FileNotFoundException, TException {
        // 1. connect to name node
        connectToNameNode();

        // 2. ask blocks
        List<Block> blocks = client2NameNodeProtocol.getFile(remoteFilePath);
        ExecutorService executorService = Executors.newFixedThreadPool(8);
        int offset = 0;
        for (Block block : blocks) {
            System.out.println(block.getId() + "   " + block.getSize());
            for (DataNodeDescriptor dataNode : block.dataNodeList) {
                if (dataNode != null) {
                    System.out.println(dataNode.getIpAddress() + ":" + dataNode.getPort());
                } else {
                    System.out.println("NULL");
                }
            }
            executorService.execute(new BlockDownloader(block.getId(), localFilePath, offset, (int)block.getSize(), block.getDataNodeList()));
            offset += block.getSize();
        }
        executorService.shutdown();
        try {
            executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            //ignore
        }

        closeConnectionWithNameNode();

    }
}
