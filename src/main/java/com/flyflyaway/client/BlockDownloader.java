package com.flyflyaway.client;

import com.flyflyaway.protocol.client2namenode.DataNodeDescriptor;

import java.io.*;
import java.net.Socket;
import java.util.List;

/**
 * Created by LinFan on 16/1/19.
 */
public class BlockDownloader extends Thread {

    private String blockId;
    private String localFilePath;
    private long startPos;
    private int blockSize;
    private List<DataNodeDescriptor> dataNodeList;


    public BlockDownloader(String blockId, String localFilePath, long startPos, int blockSize, List<DataNodeDescriptor> dataNodeList) {
        this.blockId = blockId;
        this.localFilePath = localFilePath;
        this.startPos = startPos;
        this.blockSize = blockSize;
        this.dataNodeList = dataNodeList;
    }

    private boolean downloadBlock(DataNodeDescriptor dataNode) throws IOException {
        Socket socket = new Socket(dataNode.getIpAddress(), dataNode.getPort());
        OutputStream outputStream = socket.getOutputStream();
        BlockHeader blockHeader = new BlockHeader(BlockHeader.OP_READ_BLOCK, blockId, 0, dataNodeList);
        outputStream.write(blockHeader.toBytes());
        outputStream.flush();

        boolean result = false;
        InputStream response = socket.getInputStream();
        RandomAccessFile file = new RandomAccessFile(localFilePath, "rw");
        file.seek(startPos);
        byte[] buffer = new byte[1024 * 10];
        int bytesRead;
        while ((bytesRead = response.read(buffer)) != -1) {
            file.write(buffer, 0, bytesRead);
        }
        file.close();

        response.close();
        outputStream.close();
        socket.close();

        return result;
    }

    @Override
    public void run() {
        try {
            downloadBlock(dataNodeList.get(0));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
