package com.flyflyaway.client;

import org.apache.thrift.TException;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by LinFan on 16/1/17.
 */
public class Client {
    public static void main(String[] args) {
//        Client client = new Client();
//        client.uploadFileToDFS("/Users/LinFan/Test/f.pdf");
//        client.downloadFileToLocal("/Users/LinFan/Test/f.pdf", "f.pdf");
        /*byte[] bytes = new byte[4];
        Utils.unsignedIntToBytes(204800L, bytes, 0);
        System.out.println(Utils.bytesToUnsignedInt(bytes, 0));*/
    }

    public static void uploadFileToDFS(String localFilePath, String remoteFilePath) {
        try {
            new FileUploader(localFilePath, remoteFilePath).start();
        } catch (FileNotFoundException e) {
            System.err.println("ERROR: " + localFilePath + " not found.");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TException e) {
            System.err.println("ERROR: connect to namenode.");
            e.printStackTrace();
        }
    }

    public static void downloadFileToLocal(String remoteFilePath, String localFilePath) {
        try {
            new FileDownloader(remoteFilePath, localFilePath).start();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (TException e) {
            e.printStackTrace();
        }
    }
}
