package com.flyflyaway.client;

import com.flyflyaway.protocol.client2namenode.DataNodeDescriptor;
import com.flyflyaway.util.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LinFan on 16/1/18.
 */
public class BlockHeader {

    public static final int HEADER_SIZE = 70;
    public static final int BLOCK_ID_LEN = 32;
    public static final byte OP_WRITE_BLOCK = 1;
    public static final byte OP_READ_BLOCK = 2;

    private byte op;
    private int blockSize;
    private String blockId;
    private List<DataNodeDescriptor> dataNodeList;

    public BlockHeader() {
        dataNodeList = new ArrayList<>();
    }

    public BlockHeader(byte op, String blockId, int blockSize, List<DataNodeDescriptor> dataNodeList) {
        this.op = op;
        this.blockSize = blockSize;
        this.blockId = blockId;
        this.dataNodeList = dataNodeList;
    }

    public static BlockHeader parse(byte[] bytes) {
        BlockHeader blockHeader = new BlockHeader();
        blockHeader.op = bytes[0];

        int offset = 1;
        blockHeader.blockId = new String(bytes, offset, BLOCK_ID_LEN);
        offset += BLOCK_ID_LEN;

        blockHeader.blockSize = (int) Utils.bytesToUnsignedInt(bytes, offset);
        offset += 4;

        for (int i = 0; i < 3; ++i) {
            DataNodeDescriptor dataNode = null;
            String ip = Utils.bytesToIp(bytes, offset);
            int port = (int) Utils.bytesToUnsignedInt(bytes, offset + 4);
            offset += 8;
            if (!("0.0.0.0".equals(ip) && port == 0)) {
                dataNode = new DataNodeDescriptor(ip, port);
            }
            blockHeader.dataNodeList.add(dataNode);
        }
        return blockHeader;
    }

    public byte[] toBytes() {
        byte[] bytes = new byte[HEADER_SIZE];

        // 1. op
        bytes[0] = op;
        // 2. blockId
        int offset = 1;
        byte[] blockIdBytes = blockId.getBytes();
        for (int i = 0; i < blockIdBytes.length; ++i) {
            bytes[offset++] = blockIdBytes[i];
        }
        // 3. blockSize
        Utils.unsignedIntToBytes(blockSize, bytes, offset);
        offset += 4;
        // 4. data node list
        for (int i = 0; i < 3; ++i) {
            DataNodeDescriptor dataNode = i >= dataNodeList.size() ? null : dataNodeList.get(i);
            if (dataNode == null) {
                Utils.fill(bytes, offset, 8);
            } else {
                Utils.ipToBytes(dataNode.getIpAddress(), bytes, offset);
                Utils.unsignedIntToBytes(dataNode.getPort(), bytes, offset + 4);
            }
            offset += 8;
        }
        // 5. others
        for (; offset < HEADER_SIZE; ++offset) {
            bytes[offset] = 0;
        }

        return bytes;
    }

    public byte getOp() {
        return op;
    }

    public String getBlockId() {
        return blockId;
    }

    public List<DataNodeDescriptor> getDataNodeList() {
        return dataNodeList;
    }

    public int getBlockSize() {
        return blockSize;
    }
}
