package com.flyflyaway.client;

import com.flyflyaway.protocol.client2namenode.DataNodeDescriptor;

import java.io.*;
import java.net.Socket;
import java.util.List;

/**
 * Created by LinFan on 16/1/17.
 */
public class BlockUploader extends Thread {

    private String blockId;
    private byte[] blockContent;
    private List<DataNodeDescriptor> dataNodeList;

    public BlockUploader(String blockId, byte[] blockContent, List<DataNodeDescriptor> dataNodeList) {
        this.blockId = blockId;
        this.blockContent = blockContent;
        this.dataNodeList = dataNodeList;
    }

    private boolean uploadBlock(DataNodeDescriptor dataNode) throws IOException {
        Socket socket = new Socket(dataNode.getIpAddress(), dataNode.getPort());
        OutputStream outputStream = socket.getOutputStream();
        BlockHeader blockHeader = new BlockHeader(BlockHeader.OP_WRITE_BLOCK, blockId, blockContent.length, dataNodeList);
        outputStream.write(blockHeader.toBytes());
        outputStream.flush();
        outputStream.write(blockContent);
        outputStream.flush();

        boolean result = false;
        BufferedReader response = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        String ack = response.readLine();
        if (ack != null && ack.equals("OK")) {
            result = true;
        }

        response.close();
        outputStream.close();
        socket.close();

        return result;
    }

    @Override
    public void run() {
        try {
            uploadBlock(dataNodeList.get(0));
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }
}
