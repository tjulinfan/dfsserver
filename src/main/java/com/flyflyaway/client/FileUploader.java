package com.flyflyaway.client;

import com.flyflyaway.protocol.client2namenode.Block;
import com.flyflyaway.protocol.client2namenode.Client2NameNodeProtocol;
import com.flyflyaway.protocol.client2namenode.DataNodeDescriptor;
import com.flyflyaway.util.Config;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class FileUploader {

    private static final String NAMENODE_HOST = Config.getString("namenode_host");;
    private static final int NAMENODE_PORT = Config.getInt("namenode_c2np_port");
    private static final long DEFAULT_BLOCK_SIZE = Config.getInt("block_size");

    private String localFilePath, remoteFilePath;
    private TTransport transport;
    private Client2NameNodeProtocol.Client client2NameNodeProtocol;

    public FileUploader(String localFilePath, String remoteFilePath) {
        this.localFilePath = localFilePath;
        this.remoteFilePath = remoteFilePath;
    }

    private void connectToNameNode() throws TTransportException {
        transport = new TSocket(NAMENODE_HOST, NAMENODE_PORT);
        transport.open();
        client2NameNodeProtocol = new Client2NameNodeProtocol.Client(new TBinaryProtocol(transport));
    }

    private void closeConnectionWithNameNode() {
        transport.close();
    }

    private boolean checkValidDataNodeList(List<DataNodeDescriptor> dataNodeList) {
        if (dataNodeList.size() != 3) {
            return false;
        }
        for (DataNodeDescriptor dataNode : dataNodeList) {
            if (dataNode == null) {
                return false;
            }
        }
        return true;
    }

    public void start() throws IOException, TException {
        RandomAccessFile file = new RandomAccessFile(localFilePath, "r");
        FileChannel fileChannel = file.getChannel();

        // 1. connect to namenode
        connectToNameNode();

        // 2. ask blocks
        List<Block> blocks = client2NameNodeProtocol.createFile(remoteFilePath, fileChannel.size(), DEFAULT_BLOCK_SIZE);

        long restSize = fileChannel.size();
        ExecutorService executorService = Executors.newFixedThreadPool(8);
        for (Block block : blocks) {
            System.out.println(block.getId());
            for (DataNodeDescriptor dataNode : block.dataNodeList) {
                if (dataNode != null) {
                    System.out.println(dataNode.getIpAddress() + ":" + dataNode.getPort());
                } else {
                    System.out.println("NULL");
                }
            }
            byte[] blockContent = new byte[(int)(restSize > DEFAULT_BLOCK_SIZE ? DEFAULT_BLOCK_SIZE : restSize)];
            restSize -= DEFAULT_BLOCK_SIZE;
            file.read(blockContent);
            executorService.execute(new BlockUploader(block.getId(), blockContent, block.dataNodeList));

        }
        file.close();
        executorService.shutdown();
        try {
            executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            //ignore
        }
        closeConnectionWithNameNode();
    }
}
