package com.flyflyaway.util;

/**
 * Created by LinFan on 16/1/18.
 */
public class Utils {

    // Decode the unsigned int to 4 bytes
    public static void unsignedIntToBytes(long num, byte[] bytes, int offset) {
        for (int i = 0; i < 4; ++i) {
            bytes[offset + i] = (byte)(num & 0xFF);
            num >>= 8;
        }
    }

    public static long bytesToUnsignedInt(byte[] bytes, int offset) {
        long num = 0;
        for (int i = 3; i >= 0; --i) {
            num <<= 8;
            num |= (long)bytes[offset + i] & 0xFF;
        }
        return num;
    }

    public static void ipToBytes(String ip, byte[] bytes, int offset) {
        String[] ips = ip.split("\\.");
        for (int i = 0; i < 4; ++i) {
            bytes[offset + i] = (byte)Integer.parseInt(ips[i]);
        }
    }

    public static String bytesToIp(byte[] bytes, int offset) {
        String ip = "";
        for (int i = 0; i < 4; ++i) {
            ip += String.valueOf((int)bytes[offset + i] & 0xFF);
            if (i < 3) {
                ip += ".";
            }
        }
        return ip;
    }

    public static void fill(byte[] bytes, int offset, int length) {
        for (int i = 0; i < length; ++i) {
            bytes[offset + i] = 0;
        }
    }
}
