package com.flyflyaway.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by LinFan on 16/1/19.
 */
public class Config {

    public static String CONFIG_FILE_PATH = "config.properties";
    private static Config instance = new Config(CONFIG_FILE_PATH);

    private Properties properties;
    private FileInputStream inputFile;

    private Config(String filePath) {
        properties = new Properties();
        try {
            inputFile = new FileInputStream(filePath);
            properties.load(inputFile);
            inputFile.close();
        } catch (FileNotFoundException ex) {
            System.err.println("Configuration file is not existed.");
            ex.printStackTrace();
        } catch (IOException ex) {
            System.err.println("Failed to load configuration file.");
            ex.printStackTrace();
        }
    }

    private String getStrValue(String key) {
        if (properties.containsKey(key)) {
            String value = properties.getProperty(key);
            return value;
        } else
            return "";
    }

    private int getIntValue(String key) {
        return Integer.parseInt(getStrValue(key));
    }

    public static String getString(String key) {
        return instance.getStrValue(key);
    }

    public static int getInt(String key) {
        return instance.getIntValue(key);
    }

}
