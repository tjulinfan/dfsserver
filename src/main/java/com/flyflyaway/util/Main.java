package com.flyflyaway.util;

import com.flyflyaway.client.Client;
import com.flyflyaway.datanode.DataNode;
import com.flyflyaway.exception.DataNodeStartException;
import com.flyflyaway.namenode.NameNode;
import org.apache.commons.cli.*;

import java.io.IOException;

/**
 * Created by LinFan on 16/1/20.
 */
public class Main {
    private static final String SHORT_TYPE_ARG = "t";
    private static final String SHORT_UPLOAD_ARG = "u";
    private static final String SHORT_DOWNLOAD_ARG = "d";
    private static final String SHORT_LOCAL_PATH_ARG = "l";
    private static final String SHORT_REMOTE_PATH_ARG = "r";

    public static void main(String[] args) throws ParseException, IOException, DataNodeStartException {
        Options options = new Options();
        options.addOption(SHORT_TYPE_ARG, true, "Type of running");
        options.addOption(SHORT_UPLOAD_ARG, false, "Client upload");
        options.addOption(SHORT_DOWNLOAD_ARG, false, "Client download");
        options.addOption(SHORT_LOCAL_PATH_ARG, true, "Path of local file system");
        options.addOption(SHORT_REMOTE_PATH_ARG, true, "Path of remote file system");

        CommandLine cmd = new DefaultParser().parse(options, args);
        if (cmd.hasOption(SHORT_TYPE_ARG)) {
            String type = cmd.getOptionValue(SHORT_TYPE_ARG);
            if ("namenode".equals(type)) {
                new NameNode().start();
            } else if ("datanode".equals(type)) {
                new DataNode().start();
            } else if ("client".equals(type)) {
                if (!cmd.hasOption(SHORT_LOCAL_PATH_ARG) || !cmd.hasOption(SHORT_REMOTE_PATH_ARG)) {
                    System.err.println("-" + SHORT_LOCAL_PATH_ARG + " and -" + SHORT_REMOTE_PATH_ARG + " is needed.");
                    return;
                }
                String localFilePath = cmd.getOptionValue(SHORT_LOCAL_PATH_ARG);
                String remoteFilePath = cmd.getOptionValue(SHORT_REMOTE_PATH_ARG);
                if (cmd.hasOption(SHORT_UPLOAD_ARG)) {
                    Client.uploadFileToDFS(localFilePath, remoteFilePath);
                } else if (cmd.hasOption(SHORT_DOWNLOAD_ARG)){
                    Client.downloadFileToLocal(remoteFilePath, localFilePath);
                } else {
                    System.err.println("Please use -" + SHORT_UPLOAD_ARG + " or -" + SHORT_DOWNLOAD_ARG);
                }
            } else {
                System.err.println("Unknown argument, the value should be one of [namenode | datanode | client]");
            }
        } else {
            System.err.println("Please use -" + SHORT_TYPE_ARG + " to specific the type of running.");
        }
    }
}
