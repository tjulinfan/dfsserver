package com.flyflyaway.datanode;

import com.flyflyaway.exception.DataNodeStartException;
import com.flyflyaway.protocol.datanode2namenode.DataNodeInfo;
import com.flyflyaway.protocol.datanode2namenode.DataNodeRegistration;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.thrift.TException;
import org.apache.thrift.transport.TTransportException;

import java.io.IOException;


/**
 * Created by LinFan on 16/1/11.
 */
public class DataNode {
    public static String SUB_DIR = RandomStringUtils.random(3, true, true);

    private DataServer dataServer;
    private NameNodeService nameNodeService;
    private boolean shouldRun, connectedToNameNode;
    private long lastHeartBeatTime;

    public static final long HEART_BEAT_INTERVAL = 3000;

    public DataNode() throws IOException {
        shouldRun = true;
        connectedToNameNode = false;
    }

    public void start() throws DataNodeStartException {
        startDataNode();

        while (shouldRun) {
            try {
                offerService();
                restartNameNodeService();
            } catch (TException e) {
                connectedToNameNode = false;
                restartNameNodeService();
            }
        }
        shutdownDataNode();
    }

    private boolean couldOfferService() {
        return shouldRun && connectedToNameNode;
    }

    private void offerService() throws TException {
        while (couldOfferService()) {
            long currentTime = System.currentTimeMillis();
            long lastHeartBeatTimeBackUp = lastHeartBeatTime;
            if (currentTime - lastHeartBeatTime > HEART_BEAT_INTERVAL) {
                sendHeartBeatMessage();
            }

            try {
                long restTime = HEART_BEAT_INTERVAL - (System.currentTimeMillis() - lastHeartBeatTimeBackUp);
                if (restTime > 0) {
                    Thread.sleep(restTime);
                }
            } catch (InterruptedException e) {
                // ignore
            }
        }
    }

    private void startDataServer() throws IOException {
        dataServer = new DataServer();
        dataServer.start();
    }

    private void startNameNodeService() throws TTransportException {
        nameNodeService = new NameNodeService();
        while (!nameNodeService.isAvailable()) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
            }
        }
        connectedToNameNode = true;
    }

    private void restartNameNodeService() {
        if (connectedToNameNode) {
            return;
        }

        try {
            Thread.sleep(5000);
            nameNodeService.restart();
            connectedToNameNode = true;
        } catch (TTransportException | InterruptedException e) {
            // ignore
        }
    }

    private void startDataNode() throws DataNodeStartException {
        try {
            startDataServer();
            startNameNodeService();

            registerToNameNode();
        } catch (IOException | TException e) {
            throw new DataNodeStartException(e);
        }
    }

    private void shutdownDataNode() {
        shouldRun = false;
        dataServer.shutdown();
    }

    private String getDataServerIpAddress() {
        return dataServer.getIpAddress();
    }

    private int getDataServerPort() {
        return dataServer.getLocalPort();
    }

    private long getRemainingCapacity() {
        return Utils.getUsableDiskSpace();
    }

    private void registerToNameNode() throws TException {
        DataNodeRegistration dataNodeRegistration = new DataNodeRegistration(
                getDataServerIpAddress(),
                getDataServerPort(),
                getRemainingCapacity());
        nameNodeService.registerDataNode(dataNodeRegistration);
    }

    private void sendHeartBeatMessage() throws TException {
        DataNodeInfo dataNodeInfo = new DataNodeInfo(getDataServerIpAddress(),
                getDataServerPort(),
                getRemainingCapacity());
        nameNodeService.ping(dataNodeInfo);
        lastHeartBeatTime = System.currentTimeMillis();
    }
}