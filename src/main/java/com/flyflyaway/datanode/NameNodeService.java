package com.flyflyaway.datanode;

import com.flyflyaway.protocol.datanode2namenode.DataNode2NameNodeProtocol;
import com.flyflyaway.protocol.datanode2namenode.DataNodeInfo;
import com.flyflyaway.protocol.datanode2namenode.DataNodeRegistration;
import com.flyflyaway.util.Config;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;

/**
 * Created by LinFan on 16/1/17.
 */
public class NameNodeService {

    public static final String NAMENODE_HOST = Config.getString("namenode_host");
    public static final int NAMENODE_PORT = Config.getInt("namenode_d2np_port");

    private TTransport transport;
    private DataNode2NameNodeProtocol.Client dataNode2NameNodeProtocol;

    public NameNodeService() throws TTransportException {
        connectToNameNode();
    }

    public void restart() throws TTransportException {
        System.out.println("Name node service restart.");
        connectToNameNode();
    }

    private void connectToNameNode() throws TTransportException {
        transport = new TSocket(NAMENODE_HOST, NAMENODE_PORT);
        transport.open();
        dataNode2NameNodeProtocol = new DataNode2NameNodeProtocol.Client(new TBinaryProtocol(transport));
    }

    public void registerDataNode(DataNodeRegistration dataNodeRegistration) throws TException {
        dataNode2NameNodeProtocol.registerDataNode(dataNodeRegistration);
    }

    public void ping(DataNodeInfo dataNodeInfo) throws TException {
        dataNode2NameNodeProtocol.ping(dataNodeInfo);
    }

    public boolean isAvailable() {
        return transport.isOpen();
    }
}
