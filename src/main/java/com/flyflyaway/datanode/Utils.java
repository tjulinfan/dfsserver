package com.flyflyaway.datanode;

import com.flyflyaway.util.Config;

import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by LinFan on 16/1/12.
 */
public class Utils {
    public static int BUFFER_SIZE = 1024 * 10;

    public static long getUsableDiskSpace() {
        File file = new File(Config.getString("blocks_folder"));
        if (!file.exists()) {
            file.mkdir();
        }
        return file.getFreeSpace();
    }

    public static String getLocalIpAddress() {
        try {
            InetAddress IP = InetAddress.getLocalHost();
            return IP.getHostAddress();
        } catch (UnknownHostException e) {
            // TODO: handler the exception
            return "127.0.0.1";
        }
    }

    public static void writeToLocalFile(InputStream inputStream, String path, long neededByteCount) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(path);
        byte[] buffer = new byte[BUFFER_SIZE];
        int bytesRead;
        while ((bytesRead = inputStream.read(buffer)) != -1)
        {
            fileOutputStream.write(buffer, 0, bytesRead);
            neededByteCount -= bytesRead;
            if (neededByteCount <= 0) {
                break;
            }
        }
        fileOutputStream.close();
    }

    public static void writeToLocalFileAndDataNode(InputStream inputStream, String path,
                                                   OutputStream toDataNodeStream, long neededByteCount) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(path);
        byte[] buffer = new byte[BUFFER_SIZE];
        int bytesRead;
        while ((bytesRead = inputStream.read(buffer)) != -1)
        {
            fileOutputStream.write(buffer, 0, bytesRead);
            if (toDataNodeStream != null) {
                toDataNodeStream.write(buffer, 0, bytesRead);
            }
            neededByteCount -= bytesRead;
            if (neededByteCount <= 0) {
                break;
            }
        }
        fileOutputStream.close();
    }

    public static void mkdirIfNotExists(String path) {
        File file = new File(path);
        if (!file.exists()) {
            file.mkdir();
        }
    }
}
