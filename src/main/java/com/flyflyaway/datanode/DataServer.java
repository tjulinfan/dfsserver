package com.flyflyaway.datanode;

import java.io.IOException;
import java.net.ServerSocket;

/**
 * Created by LinFan on 16/1/17.
 */
public class DataServer extends Thread {

    private ServerSocket ss;

    public DataServer() throws IOException {
        ss = new ServerSocket(0);
        System.out.println("Start transmission server at " + getLocalPort() + " ...");
    }

    @Override
    public void run() {
        while (true) {
            try {
                // TODO: implement it with thread group
                new TransmissionHandler(this, ss.accept()).start();
            } catch (IOException e) {
                // TODO: handle the exception
            }
        }
    }

    public int getLocalPort() {
        return ss.getLocalPort();
    }

    public String getIpAddress() {
        return Utils.getLocalIpAddress();
    }

    public void shutdown() {
        try {
            if (ss != null) {
                ss.close();
            }
        } catch (IOException e) {
            // ignore
        }
    }

}
