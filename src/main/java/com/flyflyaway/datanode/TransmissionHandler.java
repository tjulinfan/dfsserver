package com.flyflyaway.datanode;

import com.flyflyaway.client.BlockHeader;
import com.flyflyaway.protocol.client2namenode.DataNodeDescriptor;
import com.flyflyaway.util.Config;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.net.Socket;
import java.util.List;

/**
 * Created by LinFan on 16/1/15.
 */
public class TransmissionHandler extends Thread {

    private DataServer dataServer;
    private Socket socket;

    public TransmissionHandler(DataServer dataServer, Socket socket) {
        System.out.println("A new client is accepted, ip: " + socket.getRemoteSocketAddress());
        this.socket = socket;
        this.dataServer = dataServer;
    }

    private DataNodeDescriptor getNextTransmissionTarget(BlockHeader blockHeader) {
        List<DataNodeDescriptor> dataNodeList = blockHeader.getDataNodeList();
        int i = 0;
        for (; i < dataNodeList.size(); ++i) {
            DataNodeDescriptor dataNode = dataNodeList.get(i);
            if (dataServer != null && dataServer.getIpAddress().equals(dataNode.getIpAddress()) &&
                    dataServer.getLocalPort() == dataNode.getPort()) {
                break;
            }
        }
        if (i + 1 >= dataNodeList.size()) {
            return null;
        } else {
            return dataNodeList.get(i + 1);
        }
    }

    private void writeBlock(BlockHeader header, InputStream inputStream,
                            OutputStream responseStream,
                            InputStream fromNextDataNodeStream, OutputStream toNextDataNodeStream) throws IOException {
        final String blocksDir = Config.getString("blocks_folder") + "/" + DataNode.SUB_DIR;
        Utils.mkdirIfNotExists(blocksDir);

        if (toNextDataNodeStream != null) {
            toNextDataNodeStream.write(header.toBytes());
        }

        System.out.println("Writing block[" + header.getBlockId() + "] size: " + header.getBlockSize());
        Utils.writeToLocalFileAndDataNode(inputStream, blocksDir + "/" + header.getBlockId(),
                toNextDataNodeStream, header.getBlockSize());

        // ACK
        if (fromNextDataNodeStream != null && toNextDataNodeStream != null) {
            BufferedReader nextDNResponse = new BufferedReader(new InputStreamReader(fromNextDataNodeStream));
            String ack = nextDNResponse.readLine();
            PrintWriter writer = new PrintWriter(responseStream, true);
            if (ack != null && ack.equals("OK")) {
                writer.println("OK");
            } else {
                writer.println("FAILED");
            }
        }
    }

    private void readBlock(BlockHeader header, OutputStream outputStream) throws IOException {
        final String blocksDir = Config.getString("blocks_folder") + "/" + DataNode.SUB_DIR;

        System.out.println("Read block " + header.getBlockId());
        FileInputStream fileInputStream = new FileInputStream(blocksDir + "/" + header.getBlockId());
        IOUtils.copy(fileInputStream, outputStream);
        fileInputStream.close();
    }

    public void run() {
        try {
            InputStream inputStream = socket.getInputStream();
            OutputStream outputStream = socket.getOutputStream();

            byte[] headerBytes = new byte[BlockHeader.HEADER_SIZE];
            inputStream.read(headerBytes, 0, BlockHeader.HEADER_SIZE);
            BlockHeader header = BlockHeader.parse(headerBytes);

            switch (header.getOp()) {
                case BlockHeader.OP_WRITE_BLOCK:
                    DataNodeDescriptor nextNode = getNextTransmissionTarget(header);
                    InputStream fromNextNodeStream = null;
                    OutputStream toNextNodeStream = null;
                    Socket nextNodeSocket = null;
                    if (nextNode != null) {
                        nextNodeSocket = new Socket(nextNode.getIpAddress(), nextNode.getPort());
                        fromNextNodeStream = nextNodeSocket.getInputStream();
                        toNextNodeStream = nextNodeSocket.getOutputStream();
                    }
                    writeBlock(header, inputStream, outputStream, fromNextNodeStream, toNextNodeStream);
                    if (fromNextNodeStream != null) {
                        fromNextNodeStream.close();
                    }
                    if (toNextNodeStream != null) {
                        toNextNodeStream.close();
                    }
                    if (nextNodeSocket != null) {
                        nextNodeSocket.close();
                    }
                    break;
                case BlockHeader.OP_READ_BLOCK:
                    readBlock(header, outputStream);
                    break;
                default:
                    break;
            }

            inputStream.close();
            outputStream.close();
        } catch (IOException e) {
            // TODO: log the exception
            e.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                // Ignore this exception
            }
        }
    }
}
